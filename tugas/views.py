from django.shortcuts import render,redirect
from .forms import JadwalForm
from .models import jadwal
from django.http import HttpResponse

# Create your views here.
def home(request):
    return render(request,'pages/index.html',{})

def more(request):
    return render(request,'pages/Story3more.html',{})

def bio(request):
    return render(request,'pages/Story3bio.html',{})

def schedule(request):
    posts = jadwal.objects.all(); 
    if request.method == 'POST':
        form = JadwalForm(request.POST)
        if form.is_valid():

            u = form.save()

            return redirect('/schedule')

    else:
        form = JadwalForm

    return render(request,'pages/Jadwal.html',{'form':form,'posts': posts})

def delete(request, item):
    jadwal.objects.filter(id=item).delete()
    return redirect('/schedule')
