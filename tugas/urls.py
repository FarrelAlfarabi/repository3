from django.urls import path, include
from . import views

app_name = 'story'

urlpatterns = [
    path('',views.home,name='Home'),
    path('more/',views.more,name='More'),
    path('bio/',views.bio,name='Bio'),
    path('schedule/',views.schedule,name='Schedule'),
    path('delete/<int:item>',views.delete, name = 'delete')
]